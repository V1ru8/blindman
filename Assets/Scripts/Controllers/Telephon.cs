﻿using UnityEngine;
using System.Collections;

public class Telephon : InteractiveObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	 public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		if(type.Equals("touch"))
			c.addMessage ("This is made of plastic and has buttons on it. It might be a phone"); 
		if(type.Equals("bump"))
			c.addMessage ( "Ouch! I just kicked a low table"); 
	}
}
