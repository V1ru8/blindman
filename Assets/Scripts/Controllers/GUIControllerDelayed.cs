﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIControllerDelayed : MonoBehaviour {
	List<Message> staging;

	private static GUIControllerDelayed instance;

	void OnGUI()
	{


		//paint all messages that go onscreen
		for(int i =0; i<staging.Count; i++)
		{
			Message m = staging[i];
			m.reduceDuration(Time.deltaTime*1000);
			if(m.getDuration() <= 0f){
				GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
				c.addMessage (m.toString()); 
				staging.Remove(m);
			}
			
		}



	}
	// Use this for initialization
	void Start () {
		staging = new List<Message> ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void addMessage(string message, float delay)
	{
		staging.Add (new Message(message, delay));
	}



}
