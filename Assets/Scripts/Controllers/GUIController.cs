﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIController : MonoBehaviour {
	Queue<string> queue;
	List<Message> onScreen;
	public float durationOnScreen = 5000f;
	public int maxMessagesOnScreen = 5;
	
	int lineHeight = 30;
	int lineMargin = 10;
	int lineWidth = 600;
	private static GUIController instance;
	GUIStyle style;
	
	void OnGUI()
	{
		//Debug.Log ("GUI");
		//GUI.Box (new Rect (Screen.width-100, Screen.height/2, Screen.width, 2*lineMargin+onScreen.Count*lineHeight), "Thoughts");
		
		//add a new msg from the queue
		if (onScreen.Count < maxMessagesOnScreen && queue.Count >0) {
			
			string msg = queue.Dequeue();
			onScreen.Add(new Message(msg, durationOnScreen));
			
		}
		
		//paint all messages that go onscreen
		for(int i =0; i<onScreen.Count; i++)
		{
			Message m = onScreen[i];
			
			GUI.Label (new Rect (Screen.width/2-(m.toString().Length/2)*10, Screen.height/2-50+i*lineHeight, lineWidth, lineHeight), m.toString(),style);
			m.reduceDuration(Time.deltaTime*1000);
			if(m.getDuration() <= 0f){
				onScreen.Remove(m);
			}
			
		}
		
		
	}
	// Use this for initialization
	void Start () {
		queue = new Queue<string> (20);
		onScreen = new List<Message> ();
		lineWidth = Screen.width - lineMargin;
		style = new GUIStyle ();
		style.fontSize = 32;
		style.fontStyle = FontStyle.Bold;
		
		style.normal.textColor = Color.white;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void addMessage(string message)
	{
		queue.Enqueue (message);
	}
	

	
}
