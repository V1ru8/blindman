﻿using UnityEngine;
using System.Collections;

public class LightSwitch : InteractiveObject {
	public Light linkedLight;

	// Use this for initialization
	 void  Start () {

	
	}
	
	// Update is called once per frame
	 void Update () {
	
	}


	public override void triggerAction(string type) {
		if(linkedLight != null)
			linkedLight.enabled = !linkedLight.enabled;



		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		if(type.Equals("touch"))
		{
			c.addMessage ("I cannot find the light switch in the dark."); 
		    c.addMessage ("Better try another lamp."); 
		}
		if(type.Equals("bump"))
			c.addMessage ( "I crashed with the lamp"); 

	}


}
