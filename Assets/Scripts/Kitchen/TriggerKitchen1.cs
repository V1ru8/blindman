﻿using UnityEngine;
using System.Collections;

public class TriggerKitchen1 : InteractiveObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	public override void OnTriggerEnter(Collider other){
		base.OnTriggerEnter (other);
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		if(type.Equals("touch"))
		{
		return;
		}
		c.addMessage ( "The fusebox must be somewhere in this corner");
		cd.addMessage ( "Up on the wall!",5000f);
		DestroyObject (this);

	}
}
