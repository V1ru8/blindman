﻿using UnityEngine;
using System.Collections;

public class FuseboxController : InteractiveObject {

	public AudioSource source;
	public GameObject nextTriggger;
	int firsttime = 1;
	float countdowntoAudio = -1.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public override void OnTriggerEnter(Collider other){
		base.OnTriggerEnter (other);
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		if (type.Equals ("touch")) {
						if (firsttime == 1) {
								c.addMessage ("Strange! The fusebox seems to be allright?!");
								cd.addMessage ("Maybe...", 7000f);
								cd.addMessage ("something's wrong with me...!", 14000f);
								cd.addMessage ("Of fuck, the phone!", 21000f);
								firsttime = 0;
				source.PlayDelayed(10.0f);
				Vector3 newPos = nextTriggger.transform.position;
				newPos.z = 110;
				nextTriggger.transform.position = newPos;
								
				return;
						}
					
			c.addMessage ("The fusebox. It's working");
			source.Play();
				}

	}
}
