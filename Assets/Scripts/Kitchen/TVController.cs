﻿using UnityEngine;
using System.Collections;

public class TVController : InteractiveObject {

	public int canBeTurnedOn = 0;
	public AudioSource tvsource;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider other){

	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		if(type.Equals("touch"))
		{
			if(canBeTurnedOn == 0)
				c.addMessage ("I don't feel like 'watching' TV right now"); 
			else
			{
				c.addMessage ("let's listen to the news"); 
				tvsource.Play();
			}
		}
		if(type.Equals("bump"))
			c.addMessage ( "Oh, here's my TV"); 
	}
}
