﻿using UnityEngine;
using System.Collections;

public class TriggerHall3 : InteractiveObject {
	
	public AudioSource ring;
	public AudioSource voicemail;
	public TVController tv;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	public override void OnTriggerEnter(Collider other){
		base.OnTriggerEnter (other);
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		if(type.Equals("touch"))
		{
		return;
		}
		ring.Stop ();
		c.addMessage ( "Too late...");
		c.addMessage ( "The answering machine got it.");
		cd.addMessage ( "TV's in the kitchen...",20000f);
		tv.canBeTurnedOn = 1;
		voicemail.Play ();
		DestroyObject (this);

	}
}
