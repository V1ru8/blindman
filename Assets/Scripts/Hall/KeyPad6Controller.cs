﻿using UnityEngine;
using System.Collections;

public class KeyPad6Controller : InteractiveObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		if(type.Equals("touch"))
		{
			AudioSource audioSource = GetComponent<AudioSource> ();
			audioSource.Play ();

		}		
	}
}
