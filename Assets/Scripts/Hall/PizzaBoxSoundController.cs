﻿using UnityEngine;
using System.Collections;

public class PizzaBoxSoundController : InteractiveObject {
	
	// Use this for initialization
	void  Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	public override void OnTriggerEnter(Collider other) {
		
		base.OnTriggerEnter (other);
		AudioSource audioSource = GetComponent<AudioSource> ();
		audioSource.Play ();
		}
		
		
	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		if(type.Equals("touch"))
		{
			c.addMessage ("Pizza leftovers from last night."); 
			c.addMessage ("The kitchen must be straight ahead..."); 
		}
		if(type.Equals("bump"))
			c.addMessage ( "It smells like pizza..."); 
		
	}
}
	
	

