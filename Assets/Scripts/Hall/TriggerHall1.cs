﻿using UnityEngine;
using System.Collections;

public class TriggerHall1 : InteractiveObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	public override void OnTriggerEnter(Collider other){
		base.OnTriggerEnter (other);
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		if(type.Equals("touch"))
		{
		return;
		}
		c.addMessage ( "I guess I am in the hallway...");
		c.addMessage ("I have got to take a right turn");
		cd.addMessage ("I can't wait to have the lights back on!", 5000f);
		DestroyObject (this);

	}
}
