﻿using UnityEngine;
using System.Collections;

public class GuitarController : InteractiveObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	public override void OnTriggerEnter(Collider other){
		base.OnTriggerEnter (other);
		AudioSource audioSource = GetComponent<AudioSource> ();
		audioSource.Play ();
		//triggerAction ("bump");
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		if (type.Equals ("touch")) 
		{
			c.addMessage ("I can feel my guitar!"); 
			cd.addMessage ("the kitchen must be to the right",5000f);
		}
		if(type.Equals("bump"))
			c.addMessage ( "I don't want to break my guitar"); 

	}
}
