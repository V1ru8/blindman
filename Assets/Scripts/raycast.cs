﻿using UnityEngine;
using System.Collections;

public class raycast: MonoBehaviour {

	public Transform Effect;
	public float reach = 1.5f;
	public GameObject objectInReach;

	public bool iLookAtSomething = false;

	public Material whiteFlashMaterial;
	Material previousMaterial;
	public float flashDuration = 1.0f;

	void OnGUI()
	{
		if (iLookAtSomething) { GUI.Label(new Rect(5, Screen.height-40, 200, 30), "This feels like a " + objectInReach.name); }
	}

	// Update is called once per frame
	void Update () {
		Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast (ray, out hit, reach))
		{
			iLookAtSomething = true;



			objectInReach = hit.collider.gameObject;
			//flashObject(objectInReach);

			if(Input.GetMouseButtonDown(0))
			{
				objectInReach.SendMessage("triggerAction","touch");
			}


			Debug.DrawRay (ray.origin, Camera.main.transform.forward*reach, Color.green);

		} else {
			iLookAtSomething = false;

		}


	}

	void OnTriggerEnter()
	{

		Debug.Log ("Collision");
	}

	void OnCollisionEnter(Collision collision) 
	{
		Debug.Log ("Collision");
				foreach (ContactPoint contact in collision.contacts) {
					Debug.DrawRay(contact.point, contact.normal, Color.white);
						//TODO play audio
						contact.otherCollider.gameObject.SendMessage ("triggerAction", "bump");

				}
		}

	void OnControllerColliderHit (ControllerColliderHit hit) {
		if (hit.collider.gameObject.name != "Ground") {
			Debug.Log("controller = " +hit.controller);
			Debug.Log("gameObject = " + hit.gameObject);
			Debug.Log("transform = " +hit.transform);
			Debug.Log("hit: " +hit.collider.gameObject.name); 
		}
		
	}

	void flashObject(GameObject obj) 
	{

		Material flashMaterial = Resources.Load("Materials/matWhiteFlash") as Material;

		this.renderer.material = flashMaterial;
		//obj.AddComponent ("FlashInAndOut");
	}
}
