﻿using UnityEngine;
using System.Collections;

public class InteractiveObject : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual void triggerAction(string type) {
		
	}

	public virtual void OnTriggerEnter(Collider other){

			triggerAction("bump");

	}
}
