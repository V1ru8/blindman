﻿using UnityEngine;
using System.Collections;

public class BSGlassController : InteractiveObject {

	private bool glassFallPlayed = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider other){
		if (!glassFallPlayed) {
				AudioSource audioSource = GetComponent<AudioSource> ();
				audioSource.Play ();
				glassFallPlayed = true;
			triggerAction("bump");
		}
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		if(type.Equals("touch"))
			c.addMessage ("Glass shards are all over the place"); 
		if(type.Equals("bump"))
			c.addMessage ( "OH SHIT!! The glass dropped ont the floor"); 
	}
}
