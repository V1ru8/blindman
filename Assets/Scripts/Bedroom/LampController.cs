﻿using UnityEngine;
using System.Collections;

public class LampController : InteractiveObject {

	int firsttime=1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter(Collider other){
		AudioSource audioSource = GetComponent<AudioSource> ();
		audioSource.Play ();
		Debug.Log("Lamp Controller Trigger");
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		if(type.Equals("touch"))
		{
			if(firsttime == 1)
			{
			c.addMessage ("It seems like the lights are out"); 
			cd.addMessage ("Maybe I should check the fusebox",6000f); 
			cd.addMessage ("It's in the kitchen",12000f); 
				firsttime = 0;
			}
			else
			{
				c.addMessage ("better check the fusebox in the kitchen"); 
			}

		}
		if(type.Equals("bump"))
			c.addMessage ( "The light switch must be here"); 
	}
}
