﻿using UnityEngine;
using System.Collections;

public class HeatingController : InteractiveObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	 public override void OnTriggerEnter (Collider other)
	{
		base.OnTriggerEnter (other);
	}

	public override void triggerAction (string type)
	{
		GUIController c = FindObjectOfType(typeof(GUIController)) as GUIController;
		if(type.Equals("touch"))
			c.addMessage ("It's warm"); 
		if(type.Equals("bump"))
			c.addMessage ( "Oh, the radiator is warm"); 

		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		cd.addMessage ("test", 1000f);
	}
}
