using UnityEngine;
using System.Collections;


[RequireComponent (typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour {

	static float scale = 12.0f;

	public float mouseSensitivity = 5.0f;
	public float jumpSpeed = 20.0f;
	public Light blindLight;
	public Light mainLight;
	public GameObject playerCamera;



	float verticalRotation = 0;
	//public float upDownRange = 80.0f;
	float uprange=60.0f;
	float downrange=80.0f;
	
	float verticalVelocity = 0;

	float maxdist = scale*1.7f;
	float maxmove = 0.8f;
	float lightspeed = 2.0f;
	float lightchange = 0.3f;
	float fadeAlpha = 0.0f;

	//crouching stuff !
	int isCrouching = 0;
	float upperAngle = 40.0f;
	float upperHeight = scale*1.75f;
	float lowerAngle = 75.0f;
	float lowerHeight = scale*1.1f;

	float startTimer = 0.0f;//20.0f;


	int doorFlag = 0;

	CharacterController characterController;
	
	// Use this for initialization
	void Start () {
		Screen.lockCursor = true;
		characterController = GetComponent<CharacterController>();
		GUIControllerDelayed cd = FindObjectOfType(typeof(GUIControllerDelayed)) as GUIControllerDelayed;
		cd.addMessage ("ZZZ", 1000f);
		cd.addMessage ("ZZZ", 10000f);
		cd.addMessage ("!", 20000f);
		cd.addMessage ("Sarah?", 30000f);
		cd.addMessage ("SARAH?!", 36000f);
		cd.addMessage ("What's going on ? Why is it so dark in here?", 42000f);
		cd.addMessage ("I'll better switch the lights on...", 50000f);
		cd.addMessage ("[press WASD to move]", 60000f);
		//c.addMessage ("!", 600f);

	}
	
	// Update is called once per frame
	void Update () {
		// Rotation

		if(startTimer > 0.0f)
		{
			startTimer-=Time.deltaTime;

			return;
		}

		//if (Input.GetButton ("Fire1")) 
		//{
		//	if(mainLight.intensity > 0.5)
		//		mainLight.intensity=0.0f;
		//	else
		//		mainLight.intensity=1.0f;
		//}
		float rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
		transform.Rotate(0, rotLeftRight, 0);
		
		
		verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
		verticalRotation = Mathf.Clamp(verticalRotation, -uprange, downrange);
		Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);
		
		
		// Movement
		
		float forwardSpeed = Input.GetAxis("Vertical")  * 50.0f;
		float sideSpeed = Input.GetAxis("Horizontal")  *50.0f;
		
		verticalVelocity += Physics.gravity.y * Time.deltaTime;
		
		if( characterController.isGrounded && Input.GetButton("Jump") ) {
			verticalVelocity = jumpSpeed;
		}

		Vector3 speed = new Vector3( sideSpeed, verticalVelocity, forwardSpeed );
		
		speed = transform.rotation * speed;

		if (forwardSpeed > 0.0f) {
			AudioSource audioSource = GetComponent<AudioSource> ();
			if(!audioSource.isPlaying) {
				audioSource.Play ();
			}
		}
		
		characterController.Move( speed * Time.deltaTime );
		Transform mychildtransform = playerCamera.transform;
		

		//stuff for autocrouch
		
		float crouchState = verticalRotation;
		crouchState = (crouchState - upperAngle) / (lowerAngle - upperAngle);
		if (crouchState < 0.0f)
			crouchState = 0.0f;
		if (crouchState > 1.0f)
			crouchState = 1.0f;
		float height = lowerHeight * crouchState + upperHeight * (1.0f - crouchState);
		//Debug.Log ("height "+height);
		Vector3 heightPos = mychildtransform.transform.position;
		heightPos.y = height;
		mychildtransform.transform.position = heightPos;
		if (crouchState > 0.8 && isCrouching==0) 
		{
			//starting to crouch
			Debug.Log ("starting to crouch");
			isCrouching = 1;
		}
		if (crouchState < 0.2 && isCrouching==1) 
		{
			//starting to stand up
			Debug.Log ("standing up");
			isCrouching = 0;
		}
		
		
		//stuff for the "blindLight"
		Ray touchRay = new Ray(mychildtransform.position, mychildtransform.forward);
		RaycastHit hit;
		//ok, now we need control our touch 
		float falloffMove=maxmove;
		float fallofDist= (mychildtransform.position-transform.position).magnitude;
		if (Physics.Raycast (touchRay, out hit, maxdist)) 
		{
			Vector3 newPos = hit.point-(0.2f*scale*mychildtransform.forward);
			string str = "hit " + hit.distance;
			//Debug.Log (str);
			falloffMove=(blindLight.transform.position-newPos).magnitude*Time.deltaTime*10.0f;
			float movefactor = lightspeed*Time.deltaTime;
			if(movefactor > 0.99f)
				movefactor = 0.99f;
			blindLight.transform.position = newPos*movefactor + blindLight.transform.position*(1.0f-movefactor);
			fallofDist= (blindLight.transform.position-transform.position).magnitude;
			
		}
		if(falloffMove > maxmove)
		{
			falloffMove=maxmove;
		}
		string dm = "d=" + fallofDist + " m=" + falloffMove;
		//adjust 
		blindLight.intensity = (1.0f - falloffMove)*lightchange + blindLight.intensity*(1.0f-lightchange);
		//Debug.Log (blindLight.intensity);

		
		
	}

	void OnControllerColliderHit(ControllerColliderHit hit) {
//		Debug.Log (hit.gameObject.name);
		
		if (hit.gameObject.name == "pCube2" && blindLight.intensity < 0.5f) 
		{
			//Debug.Log (blindLight.intensity);
			doorFlag = 1;
		}
	}

	void OnGUI()
	{
		if (doorFlag==1) 
		{
//			Debug.Log ("hi");
			doorFlag = 0;
			fadeAlpha=1.0f;
		}
		DrawQuad (new Rect (0, 0, Screen.width, Screen.height), new Color (1, 0, 0, fadeAlpha));
		if (fadeAlpha > 0.0) 
		{
			fadeAlpha-=Time.deltaTime*0.1f;
		}
	}
	
	void DrawQuad(Rect position, Color color) {
		Texture2D texture = new Texture2D(1, 1);
		texture.SetPixel(0,0,color);
		texture.Apply();
		GUI.skin.box.normal.background = texture;
		string text = "";
		if (color.a > 0.01f)
			text = "You hit the door!";
		GUI.Box(position,text);
	}
}
