﻿using UnityEngine;
using System.Collections;

public class OutdoorStreetSoundsController : MonoBehaviour {

	float policeCarDriveByCountdown = 60.0f;
	bool played = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (!played) {
			if (policeCarDriveByCountdown < 0.0f) {
					AudioSource audioSource = GetComponent<AudioSource> ();
					audioSource.Play ();
					played = true;
			} else {
					policeCarDriveByCountdown -= Time.deltaTime;
			}
		}
	}
}
