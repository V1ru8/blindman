﻿using UnityEngine;
using System.Collections;

public class Message {
		float duration;
		string msg;
		
		public Message(string msg, float duration) {
			this.msg = msg;
			this.duration = duration;
		}
		
		public void reduceDuration(float milliseconds){
			this.duration -= milliseconds;
		}
		
		public float getDuration() {
			return this.duration;
		}
		
		public string toString(){
			return this.msg;
		}
		
	}
